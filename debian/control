Source: alsa-lib
Section: libs
Priority: optional
Maintainer: Debian ALSA Maintainers <pkg-alsa-devel@lists.alioth.debian.org>
Uploaders: Jordi Mallach <jordi@debian.org>,
           Elimar Riesebieter <riesebie@lxtec.de>
Build-Depends: autotools-dev,
               debhelper (>= 9),
               dh-autoreconf,
               dpkg-dev (>= 1.16.1)
Build-Depends-Indep: doxygen
Standards-Version: 3.9.5
Homepage: http://www.alsa-project.org/
Vcs-Svn: svn://anonscm.debian.org/pkg-alsa/trunk/alsa-lib
Vcs-Browser: http://anonscm.debian.org/viewvc/pkg-alsa/trunk/alsa-lib/
XS-Testsuite: autopkgtest

Package: libasound2
Architecture: linux-any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends},
         ${shlibs:Depends},
         libasound2-data (>= ${source:Version})
Suggests: libasound2-plugins (>= 1.0.24)
Breaks: bluez-alsa (<= 4.94-2),
        libasound2-plugin-equal (<= 0.6-1),
        libasound2-plugins (<< 1.0.24)
Description: shared library for ALSA applications
 This package contains the ALSA library and its standard plugins, as well
 as the required configuration files.
 .
 ALSA is the Advanced Linux Sound Architecture.

Package: libasound2-dev
Architecture: linux-any
Section: libdevel
Multi-Arch: same
Provides: libasound-dev
Depends: libasound2 (= ${binary:Version}), ${misc:Depends}
Suggests: libasound2-doc
Description: shared library for ALSA applications -- development files
 This package contains files required for developing software
 that makes use of libasound2, the ALSA library.
 .
 ALSA is the Advanced Linux Sound Architecture.

Package: libasound2-dbg
Architecture: linux-any
Multi-Arch: same
Section: debug
Priority: extra
Depends: libasound2 (= ${binary:Version}), ${misc:Depends}
Description: debugging symbols for libasound2
 This package contains the debugging symbols for the ALSA library.
 .
 Most people will not need this package.
 .
 ALSA is the Advanced Linux Sound Architecture.

Package: libasound2-data
Architecture: all
Depends: ${misc:Depends}
Suggests: alsa-utils
Breaks: libasound2 (<< 1.0.27-4)
Replaces: libasound2 (<< 1.0.27-4)
Multi-Arch: foreign
Description: Configuration files and profiles for ALSA drivers
 This package contains configuration files for ALSA drivers and UCM
 profiles for use with alsaucm.
 .
 ALSA is the Advanced Linux Sound Architecture.

Package: libasound2-udeb
Section: debian-installer
Architecture: linux-any
Depends: ${misc:Depends}, ${shlibs:Depends}
Package-Type: udeb
Description: shared library for ALSA applications (udeb)
 This package contains the ALSA library for use in the Debian Installer.
 .
 ALSA is the Advanced Linux Sound Architecture.

Package: libasound2-doc
Architecture: all
Section: doc
Depends: libjs-jquery, ${misc:Depends}
Suggests: libasound2-dev
Description: documentation for user-space ALSA application programming
 This package contains the HTML documentation for the ALSA library, which
 describes the development API for user-space applications that want to
 use ALSA.
 .
 ALSA is the Advanced Linux Sound Architecture.
